year = int(input("Please input a year:\n"))

if year % 4 == 0:
	print(f"{year} is a leap year!")
else:
	print(f"{year} is not a leap year!")

columns = int(input("Enter number of columns:\n"))
rows = int(input("Enter number of rows:\n"))

i = 0

while i < rows:
	j = 0
	while j < columns:
		print("*", end="")
		j+=1
	print()
	i+=1
